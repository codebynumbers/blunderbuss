from pprint import pprint
import simplejson as json
import math
import random

class Classifier(object):
    label_counts = {}
    label_word_counts  = {}
    testing_set = []

    def __init__(self, input, testing_portion=0):
        """ Open labeled file and train on it, possibly saving some portion for testing """
        with open(input) as fh:
            for line in fh:
                if random.random() < testing_portion:
                    self.testing_set.append(line.strip())
                else:
                    self.train(line.strip())
        if self.testing_set:
            self.test()
    
    def train(self, line):
        """ Train on single line of data at a time to avoid reading all data into memory """
        try:
            (label, data) = line.split("|")
        except:
            return
        if not self.label_word_counts.get(label):
            self.label_word_counts[label] = {}
        words = [" ".join(wl) for wl in self.ngrams(data)]
        for word in words:
            self.label_word_counts[label][word] = self.label_word_counts[label].get(word, 0) + 1
        self.label_counts[label] = self.label_counts.get(label, 0) + 1


    def test(self):
        """ Run prediction against a set of data gathered from labeled corpus """
        rows = hits = 0
        for line in self.testing_set:
            try:
                (label, data) = line.split("|")
                rows += 1
                result = self.predict(data)
                if label == result['prediction']:
                    hits += 1
            except:
                continue
        print "Accuracy: %0.f%%" % (hits/float(rows)*100)


    def predict(self, query):
        """ Make a prediction based on our model """
        scores = {}
        for label in self.label_counts.keys():
            scores[label] = 0
            words = [" ".join(wl) for wl in self.ngrams(query)]
            for word in words:
                if self.label_counts.get(label):
                    # adding natural logs better than multiple float operations
                    scores[label] += math.log(float(self.label_word_counts[label].get(word, 0.1)) / float(self.label_counts[label]))
            scores[label] += math.log(float(self.label_counts[label]) / float(sum(self.label_counts.values())))
        # Inverse the natural log
        results = [(label, math.pow(math.e, scores[label])) for label in sorted(scores, key=scores.get, reverse=True)] if scores else []
        output = { 
            'prediction': results[0][0],
            'results': results
        }
        return output

    def ngrams(self, s, n=1):
        """ Return a list of ngrams of words """
        n = int(n)
        stok = '_TK_ '
        etok = ' _TK_'
        start = stok * (n-1)
        end = etok * (n-1)
        tmp = "%s%s%s" % (start, s, end)
        tmp = self.tokenize(tmp)
        result = []
        for i in range(len(tmp)-n+1):
            result.append(tmp[i:i+n])
        return result
    
    def tokenize(self, s):
        """ Split string ... could be more advanced """
        return s.split(" ")
    
