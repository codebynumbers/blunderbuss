from classifier import Classifier


c = Classifier("data/corpus_1.txt", 0.05)


with open("data/us_keywords.txt") as infile:
    for line in infile:
        outfile = open("data/us_keywords_labeled_1.txt", "ab")
        p =  c.predict(line.strip())
        print
        print "INPUT: ", line.strip()
        print "PREDICTED:"
        for i, guess in enumerate(p['results'][:3]):
            print "%d %s %0.6f" % (i+1, guess[0], guess[1])
        choice = raw_input('Your choice, 1 - 3 or enter a new label: ')
        if choice in ["1", "2", "3"]:
            label = p['results'][int(choice)-1][0]
        else:
            label = choice
        outfile.write("%s|%s" % (label, line)) 
        outfile.close()
