from classifier import Classifier
import cherrypy
import sys
import simplejson as json

class Server(object):

    def __init__(self):
        input = sys.argv[1] if len(sys.argv) == 2 else 'input.txt'       
        self.classifier = Classifier(input, 0.1)

    @cherrypy.expose
    def index(self, q=None, **params):
        cherrypy.response.headers['Content-type'] = 'application/json' 
        return json.dumps(self.classifier.predict(q.lower()), indent=4)

    
cherrypy.quickstart(Server())


